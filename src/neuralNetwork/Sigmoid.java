package neuralNetwork;

/**
 * Created by @author austin on 2/2/15.
 */
public class Sigmoid {

    public static double sig(double x) {
	return 1 / (1 + Math.exp(-x));
    }

    public static double dSig(double x) {
	return sig(x) * (1 - sig(x));
    }
}
