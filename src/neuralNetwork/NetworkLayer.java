package neuralNetwork;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by @author austin on 2/2/15.
 */

public class NetworkLayer implements Serializable {

    private static final long serialVersionUID = 7500022295622776147L;
    private int size, inputSize;
    private Neuron[] neurons;
    private String name;
    private NetworkLayer outputLayer;

    public NetworkLayer(int size, int inputSize, String name, NetworkLayer outLayer) {
	this.size = size;
	this.inputSize = inputSize;
	this.name = name;
        outputLayer = outLayer;
	neurons = new Neuron[size];
	for (int i = 0; i < size; i++)
	    neurons[i] = new Neuron(inputSize);
    }

    protected double[] output(double[] input) {
	double[] out = new double[size];
	for (int i = 0; i < size; i++)
	    out[i] = neurons[i].activate(input);
	return out;
    }

    protected double[] feedForward(double[] input) {
        double[] out = output(input);
        return (outputLayer == null) ? out : outputLayer.feedForward(out);
    }


    protected double[] setDeltas(double[] outputError) {
        double[] deltas, inDelta;

        if (outputLayer != null) 
            deltas = outputLayer.setDeltas(outputError);
        else deltas = outputError;

        inDelta = new double[inputSize];
        Arrays.fill(inDelta, 0);

        for (int i = 0; i < size; i++) {
            double[] ds = neurons[i].setDelta(deltas[i]);
            for (int j = 0; j < inputSize; j++)
                inDelta[j] += ds[j];
        }
        return inDelta;
    }


    protected void setWeights(double[] input) {
        for (int i = 0; i < size; i++)
            neurons[i].setWeights(input);
        if (outputLayer != null)
            outputLayer.setWeights(output(input));
    }


    protected void printWeights() {
	System.out.println(name);
	for (Neuron n : neurons)
	    n.print();
    }
}
