package neuralNetwork;

import java.io.Serializable;

import flappyBird.sprites.Bird;
import flappyBird.sprites.Pipes;

/**
 * Created by @author austin on 2/2/15.
 */
public class InputLayer implements Serializable {

    private static final long serialVersionUID = 7500012295622776147L;
    private int size;
    private double[] gameInputs;
    private NetworkLayer outputLayer;

    public InputLayer(int numOfInputs, NetworkLayer outLayer) {
        outputLayer = outLayer;
	size = numOfInputs;
	gameInputs = new double[size];
    }

    public InputLayer(Bird bird, Pipes pipes, NetworkLayer outLayer) {
        outputLayer = outLayer;
	size = 2;
	gameInputs = new double[size];
	gameInputs[0] = (double) ((bird.getY() + bird.getHeight() / 2) - (pipes.getY() + pipes
		.getHeight() / 2));
	gameInputs[1] = bird.getVelocity();
    }

    public void updateInputs(Bird bird, Pipes pipes) {
	gameInputs[0] = (double) ((bird.getY() + bird.getHeight() / 2) - (pipes.getY() + pipes.getHeight() / 2));
	gameInputs[1] = bird.getVelocity();
    }

    public double[] feedForward() {
        return outputLayer.feedForward(gameInputs);
    }


    public void setDeltas(double[] error) {
        outputLayer.setDeltas(error);
    }


    public void setWeights() {
        outputLayer.setWeights(gameInputs);
    }


    protected double[] getInputs() {
	return gameInputs;
    }
}
