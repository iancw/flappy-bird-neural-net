package neuralNetwork;

import java.io.Serializable;

import flappyBird.sprites.Bird;
import flappyBird.sprites.Pipes;

/**
 * Created by @author austin on 2/2/15.
 */

public class NeuralNetwork implements Serializable {

    private static final long serialVersionUID = 7500002295622776147L;
    private InputLayer inLay;
    private NetworkLayer hidLay, outLay;
    private double output;
    private boolean flap;
    private double errAvg = 0;
    private int errCnt = 0;
    public boolean updateDisplay = false;

    public NeuralNetwork() {
	outLay = new NetworkLayer(1, 7, "Output Layer", null);
	hidLay = new NetworkLayer(7, 2, "Hidden Layer", outLay);
	inLay = new InputLayer(2, hidLay);
    }

    public void networkInput(Bird bird, Pipes pipes) {
	inLay.updateInputs(bird, pipes);
	output = feedForward();
	flap = output > .5;
    }

    public double feedForward() {
        double[] out = inLay.feedForward();
        return out[0];
    }

    public void networkLearn(Bird bird, Pipes pipes) {
        double[] error;
	if (bird.getY() < pipes.getY() + 530 || bird.getVelocity() < -2)
	    error = new double[] {0 - output};
	else if (bird.getY() > pipes.getY() + 540 && bird.getVelocity() > 0)
	    error = new double[] {1 - output};
	else return;

        updateError(error[0]);

        inLay.updateInputs(bird, pipes);
        inLay.setDeltas(error);
        inLay.setWeights();
	//hidLay.setDeltas(outLay.setDeltas(error));
	//outLay.setWeights(hidLay.setWeights(inLay.getInputs()));
    }


    private void updateError(double error) {
	errAvg = (errAvg * errCnt + Math.abs(error)) / ++errCnt;
        if (errCnt%100 == 0) updateDisplay = true;
	if (errCnt > 240) {
            Neuron.learningRate = errorFunction(errAvg);
           /*
	    if (errAvg / 13 < Neuron.learningRate) {
		Neuron.learningRate /= 2;
		updateDisplay = true;
	    }
	    if (errAvg > 13 * Neuron.learningRate) {
		Neuron.learningRate *= 1.75;
		updateDisplay = true;
		errAvg = errCnt = 0;
	    }
            */
	}
	if (errCnt > Integer.MAX_VALUE - 1)
	    errAvg = errCnt = 0;
    }


    public double errorFunction(double x) {
        return Math.pow(1000, x-1);
    }

    public double getLearningRate() {
	return Neuron.learningRate;
    }


    public boolean getFlap() {
	return flap;
    }

    // Used for debugging, commented out to prevent warnings
    /*
     * private void printWeights() { System.out.println(
     * "--------------------------------\n--------------------------------\n");
     * hidLay.printWeights();
     * //System.out.println("--------------------------------\n");
     * //hidLay2.printWeights();
     * System.out.println("--------------------------------\n");
     * outLay.printWeights(); System.out.println(
     * "--------------------------------\n--------------------------------\n");
     * }
     */

}
