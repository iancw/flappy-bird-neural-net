package neuralNetwork;

import static neuralNetwork.Sigmoid.dSig;
import static neuralNetwork.Sigmoid.sig;

import java.io.Serializable;

/**
 * Created by @author austin on 1/30/15.
 */

public class Neuron implements Serializable {

    private static final long serialVersionUID = 7500032295622776147L;
    public static double learningRate = 0.003;

    private int inputSize;
    private double[] weights;
    private double delta;

    // Bias is the last value in the weights array
    public Neuron(int inputSize) {
	this.inputSize = inputSize + 1;
	weights = new double[inputSize];
	for (int i = 0; i < inputSize; i++)
	    weights[i] = Math.random() - 0.5;
    }

    protected double activate(double inputs[]) {
	double out = weights[weights.length - 1];
	for (int i = 0; i < inputs.length; i++)
	    out += inputs[i] * weights[i];
	return sig(out);
    }

    protected double activateDir(double[] inputs) {
	double out = weights[weights.length - 1];
	for (int i = 0; i < inputs.length; i++)
	    out += inputs[i] * weights[i];
	return dSig(out);
    }

    protected double[] setDelta(double delta) {
	this.delta = delta;
	double[] inputDeltas = new double[inputSize - 1];
	for (int i = 0; i < inputDeltas.length; i++)
	    inputDeltas[i] = weights[i] * delta;
	return inputDeltas;
    }

    protected void setWeights(double[] inputs) {
	double df = activateDir(inputs);
	weights[weights.length - 1] += (learningRate * delta * df);
	for (int i = 0; i < inputs.length; i++)
	    weights[i] += (learningRate * delta * df * inputs[i]);
    }

    protected void print() {
	for (double w : weights)
	    System.out.println(w);
    }
}
