package flappyBird.game;

public class Enums {
    public enum GameState {
	START, PLAY, DYING, DEAD, MENU, PAUSE, EXIT
    }
    
    public enum GameMode {
	USER, MACHINE, MULTIMACHINE
    }


    public enum BirdState {
	ALIVE(1), DYING(2), DEAD(3);
	private final int val;
	BirdState(int val) { this.val = val; }
	public int getValue() { return val; }
    }
    
    public enum Buttons{
	PLAYB, PAUSEB, SETTINGSB, MENUB, RESUMEB, CONFIRMB, CANCELB, HIGH_SCOREB, LOADB
    }
    
    public enum Menu{
	MAIN, PAUSE, SETTINGS, CONFIRM, DEATH, HIGH_SCORE, NONE
    }
}
