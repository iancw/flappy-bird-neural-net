
package flappyBird.game;


import flappyBird.game.Enums.BirdState;
import flappyBird.sprites.*;
import flappyBird.sprites.menu.HighScoreMenu;
import flappyBird.sprites.menu.DeathMenu;
import flappyBird.sprites.menu.MButton;
import flappyBird.sprites.menu.MainMenu;
import flappyBird.sprites.menu.PauseMenu;
import flappyBird.sprites.menu.ConfirmMenu;
import flappyBird.sprites.menu.SettingsMenu;
import flappyBird.sprites.menu.Menu;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * To increase learning speed multiples instances of Bird could be used. This
 * could be done with only minor changes by replacing Bird bird with
 * ArrayList<Bird> birds, and updating the methods to iterate across the list.
 */

public class GameAssets {

    private ArrayList<Collidable> collidables;
    private ArrayList<Bird> birds;
    private String[] birdColor;
    private Background background;
    // added 20150423, variables for new graphcial elements
    private final MainMenu mainMenu;
    private final PauseMenu pauseMenu;
    private final ConfirmMenu confirmMenu;
    private final DeathMenu deathMenu;
    private final HighScoreMenu highScoreMenu;
    private final SettingsMenu settingsMenu;
    private Menu currentMenu;

    // All of the /img/ file reading is called from this constructor
    // All of the image files themselves are loaded in Sprite

    public GameAssets() {
	collidables = new ArrayList<Collidable>();
	collidables.add(new Pipes("img/pipes.png"));
	collidables.add(new Pipes(collidables.get(collidables.size() - 1).getImage()));
	collidables.add(new Ground("img/ground.png"));
	collidables.add(new Ground(collidables.get(collidables.size() - 1).getImage()));
	birds = new ArrayList<Bird>();
	birds.add(new Bird("img/Bird/blue/bird.png"));
	background = new Background();
	Sprite.loadAnimations();
	
	birdColor = new String[6];
	birdColor[0] = "img/Bird/blue/";
	birdColor[1] = "img/Bird/yellow/";
	birdColor[2] = "img/Bird/green/";
	birdColor[3] = "img/Bird/grey/";
	birdColor[4] = "img/Bird/purple/";
	birdColor[5] = "img/Bird/red/";
	
	// added 20150423

	// instantiate all menus
	mainMenu = new MainMenu(Enums.Menu.MAIN);
	pauseMenu = new PauseMenu(Enums.Menu.PAUSE);
	confirmMenu = new ConfirmMenu(Enums.Menu.CONFIRM);
	deathMenu = new DeathMenu(Enums.Menu.DEATH);
	settingsMenu = new SettingsMenu(Enums.Menu.SETTINGS);
        highScoreMenu = new HighScoreMenu(Enums.Menu.HIGH_SCORE);

        deathMenu.updateMenuScore(0);
        birds.get(0).getScoreDisplay().updateScore(0);
    }

    public boolean resetDeathMenu() {
        return deathMenu.reset;
    }
    
    public void spawnMoreBirds(int numberOfBirdsDesired) {
	for(int i = 0; i < numberOfBirdsDesired; i++)
	    birds.add(new Bird());
    }
    
    public void killExtraBirds() {
	while(birds.size() > 1)
	    birds.remove(birds.get(1));
    }

    // Used to stop the bird's animation. Eventually this will handle all sprite
    // animations
    public void stopAnimation() {
	for(Bird b : birds)
	    b.stopAnim();
    }

    // Updates all moving Sprites
    protected void updateSprites() {
	for (Collidable collidable : collidables)
	    collidable.update();
	for(Bird b : birds)
	    b.update();
	background.update();
    }
    
    protected void updateBird(Bird b, Pipes pipes) {
	//System.out.println("Bird: " +  (b.x + b.getWidth()) + "Pipes: " + (pipes.x));
	//boolean safeToSpawn = (b.x + b.getWidth() < pipes.x);
	b.updateAndRespawn(birdIsSafeToSpawn(b,pipes));
    }
    
    protected boolean birdIsSafeToSpawn(Bird b, Pipes pipes) {
	return (b.x + b.getWidth() < pipes.x);
    }
    
    protected void updateCollidables() {
	for (Collidable col : collidables) {
	    col.update();
	}
	background.update();
    }
    
    protected void updateBackground() {
	background.update();
    }

    // Renders all of the Sprites and the score
    protected void renderSprites(Graphics2D g) {
	background.render(g);
	for (Collidable collidable : collidables)
	    collidable.render(g);
	for(Bird b : birds)
	    b.render(g);
    }

    // Method is called from keyAdapter and mouseAdapter event handlers
    protected void flapBird() {
	getBird().flap();
    }

    public void updateHighScoreMenu(List<String> scoreList) {
        highScoreMenu.setScoreList(scoreList);
    }
    
    protected void flapBird(Bird b) {
	b.flap();
    }


    protected void detectAllCollisions() {
	for (Collidable col : collidables) {
	    for(Bird bird : birds) {
		detectCollision(bird,col);
	    }
	}
    }

    
    protected void detectBirdCollisions(Bird bird) {
	for (Collidable col : collidables) {
	    detectCollision(bird,col);
	}
    }
    
    protected void detectCollision(Bird bird, Collidable col) {
	if(bird.getBirdState() != BirdState.DEAD) { // todo: this shouldn't be needed
	    if(col.birdCollided(bird)) {
		bird.doCollision(col);
	    }
	    else {
		bird.updateScore(col);

	    }
	}
    }

    protected Enums.GameState updateGameState() {
	BirdState birdsState = BirdState.DEAD;
	for(Bird b : birds) {
	    if(b.getBirdState().getValue() < birdsState.getValue())
		birdsState = b.getBirdState();
	}
	    
	switch (birdsState) {
	case ALIVE:
	    return Enums.GameState.PLAY;
	case DYING:
	    return Enums.GameState.DYING;
	case DEAD:
	    return Enums.GameState.DEAD;
	default:
	    return Enums.GameState.PLAY;
	}
    }
    
    public void renderScore(Graphics2D g) {
	Bird maxBird = null;
	for(Bird b : birds) {
	    if(maxBird == null)
		maxBird = b;
	    if(b.score > maxBird.score)
		maxBird = b;
	}
        deathMenu.updateMenuScore(birds.get(0).getScoreDisplay().getDisplayedScore());
	maxBird.renderScore(g);
        //highScoreMenu.setScore(DmaxBird.getDisplayedScore().getDigitList());
    }

    
    // Resets the Sprites when gameState changes from Dead to Play
    protected void resetSprites() {
        deathMenu.setScore(birds.get(0).getScoreDisplay().getDigitList());
	for (int i = 0; i < collidables.size(); i++)
	    collidables.get(i).reset(i % 2);

	for(Bird b : birds)
	    b.reset();
        birds.get(0).getScoreDisplay().updateScore(0);
    }


    protected Bird getBird() {
	return birds.get(0);
    }
    

    protected ArrayList<Bird> getBirds() {
	return birds;
    }


    protected Pipes getPipes() {
	if (collidables.get(0).getX() + collidables.get(0).getWidth() < getBird().getX())
	    return (Pipes) collidables.get(1);
	else if (collidables.get(1).getX() + collidables.get(1).getWidth() < getBird().getX())
	    return (Pipes) collidables.get(0);
	return (collidables.get(0).getX() <= collidables.get(1).getX()) ? (Pipes)collidables.get(0) : (Pipes)collidables.get(1);
    }


    public void renderMenu(Graphics2D g) {
	if(currentMenu != null)
	    currentMenu.render(g);

    }
    

    public void updateButtonHover(int mouseX, int mouseY){
	if(currentMenu != null)
            currentMenu.updateButtonHover(mouseX, mouseY);
    }


    public void updateButtonPress(int mouseX, int mouseY) {
        if (currentMenu != null)
            currentMenu.updateButtonPress(mouseX, mouseY);
    }


    public MButton checkButtonRelease(int mouseX, int mouseY) {
        if (currentMenu == null) return null;
        return currentMenu.checkButtonRelease(mouseX, mouseY);
    }
    

    protected int getBirdCount() {
        int cnt = 0;
        for (Bird b : birds) 
            if (b.getBirdState() == Enums.BirdState.ALIVE)
                cnt++;
        return cnt;
    }
      
    
    public void setMenu(Enums.Menu input){
	switch(input){
		case PAUSE:
		    currentMenu = pauseMenu;
		    break;
    		case MAIN:
    		    currentMenu = mainMenu;
    		    break;   
    		case CONFIRM:
    		    currentMenu = confirmMenu;  
    		    break;
    		case DEATH:
    		    currentMenu = deathMenu;
    		    break;
                case HIGH_SCORE:
                    currentMenu = highScoreMenu;
                    break;
                case SETTINGS:
                    currentMenu = settingsMenu;
                    break;
    		case NONE:
    		    currentMenu = null;
    		    break;
    		default:
    		    break;
    	}
    }
    
    public Enums.Menu getMenuType(){
	if(currentMenu != null)
	    return currentMenu.getmType();
	return Enums.Menu.NONE;
    }


}
