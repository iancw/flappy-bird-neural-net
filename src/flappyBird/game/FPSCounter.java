package flappyBird.game;

public class FPSCounter {
    static double fps = 0;
    static double lastFPS = 0;
    static double updatedTime = 0;
    static double lastTime = 0;
    static double delta = 1000 / 60;
    static int interval = 6000;

    FPSCounter(double delta, int intervalMiliseconds) {
	this.delta = delta;
	this.interval = intervalMiliseconds;
    }

    // returns -1 if the fps needs to be updated (based on time interval).
    // Return the FPS if it does.
    int getFPS(double currentTime) {
	int returnVal = -1;

	if (currentTime - lastTime >= delta) {
	    if ((currentTime - updatedTime) >= interval) {
		lastFPS = (fps * (double) (1000 / (currentTime - lastTime)));
		returnVal = (int) lastFPS;
		updatedTime = currentTime;
	    }
	    lastTime = currentTime;
	    fps = 1;
	} else {
	    fps++;
	}

	return returnVal;
    }
}
