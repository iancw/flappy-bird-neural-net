package flappyBird.game;
import flappyBird.game.Enums.*;
import flappyBird.sprites.*;
import flappyBird.sprites.menu.*;
import neuralNetwork.NeuralNetwork;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferStrategy;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class FlappyBird implements Runnable {

    private NeuralNetwork neuralNetwork;
    private Thread thread;
    private Canvas canvas;
    private BufferStrategy bs;
    private JFrame frame;
    private GameAssets gameAssets;
    private GameState gameState;
    private Highscores highscores;
    private boolean scoresUpdated = false;
    private long deathTimer = 0;
    private long deathTimerMax = 0;
    private boolean spacePressed = false;
    private boolean escPressed = false;
    private GameMode gameMode;
    private double learningRate = 0;
    private MButton bClicked = null;
    private boolean spawnBirds = false;
    private boolean killBirds = false;
    private Font font;

    private static final int HEIGHT = 720, WIDTH = HEIGHT*10/16; // for aspect ratio of 10x16, this gives width of 450
    private static final int UPS = 60; // updates per second
    private static int FPS = 0;
    private static int aliveTime = 0;

    public FlappyBird() {
	neuralNetwork = new NeuralNetwork();
	learningRate = neuralNetwork.getLearningRate();
	
	gameState = GameState.START;
	gameMode = GameMode.USER;
	gameAssets = new GameAssets();
	gameAssets.setMenu(Enums.Menu.MAIN);

	frame = new JFrame("AutoBirds, roll out!"); 
	
	JPanel frameContents = (JPanel) frame.getContentPane();
	frameContents.setPreferredSize(new Dimension(WIDTH, HEIGHT));
	frameContents.setLayout(null); 

	canvas = new Canvas();
	canvas.setBounds(0, 0, WIDTH, HEIGHT);
	canvas.setIgnoreRepaint(true);
	frameContents.add(canvas);
	
	try {
	    font = Font.createFont(Font.TRUETYPE_FONT, new File("font/OpenSans-Bold.ttf"));
	} catch (FontFormatException e1) {
	    e1.printStackTrace();
	} catch (IOException e1) {
	    e1.printStackTrace();
	}
	
	GraphicsEnvironment ge = 
	            GraphicsEnvironment.getLocalGraphicsEnvironment();
	ge.registerFont(font);
        font = new Font("Open Sans", Font.BOLD , 16);
	
	// Click or press space for the bird to "flap"
	canvas.addMouseListener(new MouseAdapter() {
	    @Override
	    public void mousePressed(MouseEvent e) {
		switch(gameState){
		case PAUSE:
		case MENU:
                    gameAssets.updateButtonPress(e.getX(), e.getY());
		    break;
		case PLAY:
		    gameAssets.flapBird();
		    break;
		case DYING:
		    break;
		case DEAD:
		    //deathTimerMax = 0;
                    gameAssets.updateButtonPress(e.getX(), e.getY());
                    if (gameAssets.resetDeathMenu())
                        reset();
		    break;
		case EXIT:
		    break;
		default:
		    break;

		}
	    }
            @Override
            public void mouseReleased(MouseEvent e) {
                if (gameState == Enums.GameState.DEAD || gameState == Enums.GameState.MENU || gameState == Enums.GameState.PAUSE) {
		    bClicked = gameAssets.checkButtonRelease(e.getX(), e.getY());
                }
            }
	});
	
	canvas.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
        	gameAssets.updateButtonHover(e.getX(), e.getY());
            }
        });
	
	canvas.addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
		    spacePressed = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
		    escPressed = false;
	    }

	    @Override
	    public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_F) {

		}
		if (e.getKeyCode() == KeyEvent.VK_SPACE && !spacePressed) {
		    if(gameState == GameState.PLAY)
			gameAssets.flapBird();
		    else if(gameState == GameState.DEAD)
			deathTimerMax = 0;
		    spacePressed = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_M && gameState == GameState.PLAY){
		    toggleMachineLearning();
		}
		if(e.getKeyCode() == KeyEvent.VK_K && gameState == GameState.PLAY){
		    changeGameMode(GameMode.MULTIMACHINE);
		}
		if(e.getKeyCode() == KeyEvent.VK_J && gameState == GameState.PLAY){
		    changeGameMode(GameMode.USER);
		}
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE && !escPressed && gameState == GameState.PLAY){		    
		    escPressed = true;
		    pause();
		}
		/*
		 * Escape has different functionality at different points in the game
		 * If you are currently playing, esc brings up the pause menu
		 * If you are in the pause menu, esc brings you back to the game
		 * If you are in the confirm menu after pressing menu in the pause menu, esc
		 * ...takes you back to the pause menu
		 * The else if statement prevents hold esc from opening and then closing the pause menu
		 */
		else if(e.getKeyCode() == KeyEvent.VK_ESCAPE && gameState == GameState.PAUSE){
		    if(gameAssets.getMenuType() == Enums.Menu.CONFIRM)
			gameAssets.setMenu(Enums.Menu.PAUSE);
		    else if(!escPressed)
			unPause();
		    escPressed = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_S && gameState == GameState.PLAY && isMachinePlay()) {
		    // TODO: add saving and loading notification to these when L and S are pressed
		    pause();
		    JFileChooser fc = new JFileChooser();
		    fc.setCurrentDirectory(new File("saves"));
		    int option = fc.showSaveDialog(frame);
		    String saveFileName = null;
		    if (option == JFileChooser.APPROVE_OPTION)
			saveFileName = fc.getSelectedFile().getPath();
		    if (saveFileName != null) {
			if (!saveFileName.endsWith(".ser"))
			    saveFileName += ".ser";
			SaveLearning(neuralNetwork, saveFileName);
		    }
		    unPause();
		}
		if(e.getKeyCode() == KeyEvent.VK_L && gameState == GameState.PLAY && isMachinePlay()) {
		    pause();
		    JFileChooser fc = new JFileChooser();
		    fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		    fc.setCurrentDirectory(new File("saves"));
		    String loadFileName = null;
		    int option = fc.showOpenDialog(frame);
		    if (option == JFileChooser.APPROVE_OPTION)
			loadFileName = fc.getSelectedFile().getPath();
		    if (loadFileName != null && loadFileName.endsWith(".ser"))
			neuralNetwork = LoadLearning(neuralNetwork, loadFileName);
		    unPause();
		}
	    }
	});

	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.pack();
	frame.setResizable(false);
	frame.setLocationRelativeTo(null); // Places JFrame in the center of screen
	frame.setVisible(true);

	canvas.createBufferStrategy(2);
	bs = canvas.getBufferStrategy();
        
	// load highscores in new thread
	highscores = new Highscores();
        gameAssets.updateHighScoreMenu(highscores.getDisplayStrings());
	
    }


    // This function starts the running loop
    private void start() {
	gameState = GameState.MENU;
	canvas.requestFocus();
	thread = new Thread(this);
	thread.start();

    }


    // Stops the program much more gracefully.
    private void stop() {
	if (thread == null || !thread.isAlive())
	    return;
	thread.interrupt();
	try {
	    thread.join();
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }



    // Contains the game running loop that continuously calls render and update when appropriate
    public void run() {
	double currentTime, lastTime, delta;
	lastTime = System.currentTimeMillis();
	delta = 1000/UPS;
	FPSCounter fpser = new FPSCounter(delta,600);

	
	while (!thread.isInterrupted()) {
	    currentTime = System.currentTimeMillis();

	    int fps = fpser.getFPS(currentTime);
	    if (fps >= 0)
		FPS = fps;

	    // UPS = updates per second. delta = 1000/UPS = the amount of time
	    // in milliseconds between updates
	    if (currentTime - lastTime >= delta) {
		update();
		aliveTime += (int) (currentTime - lastTime);
		lastTime = currentTime;
	    }   

            render();
            if (currentTime-lastTime < 10) {
                try {
                    Thread.sleep((long)(delta-(currentTime-lastTime)-5));
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
	}
    }


    // The images themselves are drawn on in the Sprite class
    public void render() {
	Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        g.setFont(font);
	g.clearRect(0, 0, WIDTH, HEIGHT); //Clears the previous frame
	switch (gameState) {
	    case MENU:
		gameAssets.renderMenu(g);
		break;
	    case PLAY:
	    case DYING:
	    case EXIT:
		gameAssets.renderSprites(g);
		gameAssets.renderScore(g);
                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		drawText(g, 0, 0,  statsList()); // draw stats
		break;
	    case DEAD:
	    case PAUSE:
		gameAssets.renderSprites(g); 
		gameAssets.renderMenu(g);
		break;
	    default:
	    }
	//Draws all of the images in gameImages instance ArrayList<Sprite>
	
	g.dispose();
	bs.show();
    }



    // Handles game engine actions based on gameImages and gameState
    public void update() {
	switch (gameState) {
	case MENU:
	    //this needs to change the rendering
	    if(bClicked != null){
		switch(bClicked.getType() ){
			case PLAYB:
			    gameState = Enums.GameState.PLAY;
			    gameAssets.setMenu(Enums.Menu.NONE);
			    // Reset bClicked so update() doesn't try to set gamestate again
			    bClicked = null;
			    break;
			case SETTINGSB:
			    gameAssets.setMenu(Enums.Menu.SETTINGS);
			    bClicked = null;
			    break;
			case MENUB:
			    gameAssets.setMenu(Enums.Menu.MAIN);
			    bClicked = null;
			    break;
                        case HIGH_SCOREB:
                            gameAssets.setMenu(Enums.Menu.HIGH_SCORE);
                            bClicked = null;
                            break;
			case LOADB:
			    loadNetwork();
			    bClicked = null;
			    break;

			default:
			    bClicked = null;
			    break;
		}
	    }
	    //gameState=GameState.PLAY;
	    break;
	
		
	case PAUSE:
	    if(bClicked != null){
		switch(bClicked.getType() ){
			case MENUB:
			    gameAssets.setMenu(Enums.Menu.CONFIRM);	
			    // Reset bClicked so update() doesn't try to set gamestate again
			    bClicked = null;
			    break;
			case RESUMEB:
			    gameState = Enums.GameState.PLAY;
			    gameAssets.setMenu(Enums.Menu.NONE);
			    bClicked = null;
			    break;
			case CONFIRMB:
			    gameState = Enums.GameState.MENU;
			    gameAssets.setMenu(Enums.Menu.MAIN);
			    reset();
			    bClicked = null;
			    break;
			case CANCELB:
			    gameAssets.setMenu(Enums.Menu.PAUSE);
			    bClicked = null;
			    break;
			default:
			    bClicked = null;
			    break;
		}
	    }
	    //gameState=GameState.PLAY;
	    break;
	    
	case PLAY:
	    // these need to be done outside of a bird
	    tryKillExtraBirds();
	    trySpawnExtraBirds(5);
	    
	    Pipes pipes = gameAssets.getPipes();
	    for(Bird b : gameAssets.getBirds()) {
                if (b.getBirdState() == Enums.BirdState.DEAD) {
                    gameAssets.updateBird(b,pipes);
                    continue;
                }
                
		// input to NN
		if (isMachinePlay())
		    neuralNetwork.networkInput(b, pipes);

		// update position and detect collision of birds
		gameAssets.updateBird(b,pipes);
		gameAssets.detectBirdCollisions(b);

		// teach the NN
		if (isMachinePlay()) {
		    if (neuralNetwork.updateDisplay) {
			learningRate = neuralNetwork.getLearningRate();
			neuralNetwork.updateDisplay = false;
		    }
		    neuralNetwork.networkLearn(b, pipes);
		    if (neuralNetwork.getFlap())
			gameAssets.flapBird(b);
		}
	    }
	
	    gameAssets.updateCollidables();
	    gameState = gameAssets.updateGameState();
	    break;
	case DYING:
	    // TODO: play death animation for other deaths
	    gameState = gameAssets.updateGameState();
	    break;
	case DEAD:
	    // TODO: Game Over: new game / high score / quit menu
            if (gameAssets.getMenuType() == Enums.Menu.DEATH && bClicked != null) {
                if (bClicked.getType() == Enums.Buttons.HIGH_SCOREB) {
                    gameAssets.setMenu(Enums.Menu.HIGH_SCORE);
                    gameState = GameState.MENU;
                }
                if (bClicked.getType() == Enums.Buttons.MENUB) {
                    gameAssets.setMenu(Enums.Menu.MAIN);
                    gameState = GameState.MENU;
                }
            }
            if (!scoresUpdated && gameAssets.getMenuType() == null) {
		if (highscores.addScore(gameAssets.getBird().getScoreDisplay().getDisplayedScore(), isMachinePlay(), ""))
                    gameAssets.updateHighScoreMenu(highscores.getDisplayStrings());
                scoresUpdated = true;
            }
	    if (deathTimer == 0) {
		deathTimerMax = isMachinePlay() ? 0 : 3;
		deathTimer = System.currentTimeMillis();
	    } 
            else {		
                     scoresUpdated = false;
                     if (isMachinePlay()) {
                         gameAssets.setMenu(Enums.Menu.NONE);
                         //sleep to keep animation from playing after respawn for the most part
                         try {
                             Thread.sleep(200);
                         } catch (InterruptedException ie) {
                             ie.printStackTrace();
                         }
			 reset();
                     }
		     else {
			 if(gameAssets.getMenuType() == Enums.Menu.NONE )
			     gameAssets.setMenu(Enums.Menu.DEATH);
		     }
	    }
	    
	    break;
	case EXIT:
	    stop(); // will cause the main loop to end
	    break;
	    
	default:
	    gameAssets.updateSprites();
	    break;
	}
    }
    
    public void SaveLearning(NeuralNetwork nn, String fileName) {
	// save serialized neural network state to a file
	FileOutputStream fileOut;
	try {
	    //fileOut = new FileOutputStream("learn_" + java.util.UUID.randomUUID() + ".ser");
	    fileOut = new FileOutputStream(fileName); // using one file

	    ObjectOutputStream out;
	    try {
		out = new ObjectOutputStream(fileOut);
		out.writeObject(nn);
		out.close();
	    } catch (IOException e1) {
		e1.printStackTrace();
	    }

	    try {
		fileOut.close();
	    } catch (IOException e1) {
		e1.printStackTrace();
	    }
	} catch (FileNotFoundException e2) {
	    e2.printStackTrace();
	}
    }


    public NeuralNetwork LoadLearning(NeuralNetwork nn, String fileName) {
	// load serialized neural network state from a file
	try
	{
	    FileInputStream fileIn = new FileInputStream(fileName);
	    ObjectInputStream in = new ObjectInputStream(fileIn);
	    NeuralNetwork loadedNetwork = (NeuralNetwork) in.readObject(); // load the serialized object
	    in.close();
	    fileIn.close();
	    return loadedNetwork;
	}catch(IOException i)
	{
	    i.printStackTrace();
	}catch(ClassNotFoundException c)
	{
	    System.out.println("NeuralNetwork class not found");
	    c.printStackTrace();
	}
	return nn;
    }


    // Resets all the relevant gameImages instances after the bird dies
    public void reset() {
	spacePressed = false;
	gameAssets.resetSprites();
	aliveTime = 0;
	try {
	    Thread.sleep(50);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	if(gameState == GameState.DEAD)
	    gameState = GameState.PLAY;
    }


    // Pause method: at the moment, all it does is stop any bird animations 
    public void pause() {
	gameAssets.setMenu(Enums.Menu.PAUSE);
	gameState = GameState.PAUSE;
	gameAssets.stopAnimation(); // Stop the animation
    }
    
    public void unPause() {
	gameState = GameState.PLAY;
	gameAssets.setMenu(Enums.Menu.NONE);
    }


    // Generates the string to be stored in the top right corner of the screen
    public List<String> statsList() {
	List<String> stats = new ArrayList<String>();

	if (isMachinePlay())
	    stats.add(String.format("Learning Rate: %.5f", learningRate));

	//Machine play state
	String str = "Machine Play is ";
	str += (isMachinePlay()) ? "ON" : "OFF";
	stats.add(str);

        //Bird count if more than one
        if (gameMode == Enums.GameMode.MULTIMACHINE)
            stats.add("Bird Count: " + gameAssets.getBirdCount());

	//Time played
	int time = aliveTime/1000;
	str = "Time Played: ";
	if (time > 3600) {
	    str += Integer.toString(time/3600) + ":";
	    time %= 3600;
	    if (time/60 < 10) str += "0";
	}
	str += Integer.toString(time/60) + ":";
	if (time%60 < 10) str += "0";
	str += Integer.toString(time%60);
	stats.add(str);

	//Frames per second
	stats.add("FPS: " + Integer.toString(FPS));

	return stats;
    }
    
    // draws text from a list directly to screen
    private static void drawText(Graphics2D g, int xOffset, int yOffset, List<String> lines) { 
	for (int i = 1; i <= lines.size(); i++)
	    g.drawString(lines.get(i-1), 5+xOffset, i*18+yOffset);
    }
    
    public List<String> keybindingDisplay() {
	List<String> stats = new ArrayList<String>();
	stats.add("Pause: ESC");
	stats.add("Machine Mode: M");
	stats.add("Multimachine: K");
	stats.add("User Mode: J");
	stats.add("Save Learning: S");
	stats.add("Load Learning: L");
	
	return stats;
    }
    
    private boolean isMachinePlay() {
	return gameMode != GameMode.USER;
    }


    // All main does is create an instance of FlappyBird, and call the start() method.
    public static void main(String[] args) {
	FlappyBird flappyBird = new FlappyBird();
	flappyBird .start();
    }
    
    public void toggleMachineLearning(){
	if(isMachinePlay())
	    changeGameMode(GameMode.USER);
	else{
	    changeGameMode(GameMode.MACHINE);
	    gameAssets.setMenu(Enums.Menu.NONE);
	}
    }
   
    public void changeGameMode(GameMode m) {
	switch(m) {
	case MACHINE:
	    killBirds = true;
	    break;
	case MULTIMACHINE:
	    spawnBirds = true;
	    killBirds = false;
	    break;
	case USER:
	    killBirds = true;	    
	    break;
	default:
	    break;
	
	}
	gameMode = m;
    }
    
    private void tryKillExtraBirds() {
	if(killBirds) {
	    gameAssets.killExtraBirds();
	    killBirds = false;
	}
    }
    
    private void trySpawnExtraBirds(int numBirds) {
	if(spawnBirds) {
	    gameAssets.spawnMoreBirds(numBirds);
	    spawnBirds = false;
	}
    }
    
    private void loadNetwork(){
	    JFileChooser fc = new JFileChooser();
	    fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	    fc.setCurrentDirectory(new File("saves"));
	    String loadFileName = null;
	    int option = fc.showOpenDialog(frame);
	    if (option == JFileChooser.APPROVE_OPTION)
		loadFileName = fc.getSelectedFile().getPath();
	    if (loadFileName != null && loadFileName.endsWith(".ser"))
		neuralNetwork = LoadLearning(neuralNetwork, loadFileName);
    }
}
