package flappyBird.game;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;


public class Highscores {

    private String fileName = "saves/highscores/highscores.txt";
    private Score[] scores;

   
    public Highscores() {
        scores = new Score[15];
        try {
            Scanner in = new Scanner(new File(fileName));
            int i = 0;
            while (in.hasNextLine() && i < scores.length) {
                String[] line = in.nextLine().split(":");
                scores[i++] = new Score(Integer.parseInt(line[0]), Boolean.parseBoolean(line[1]), line[2]);
            }
            in.close();
        } catch (FileNotFoundException fnfe) {
            System.out.println("saves/highscores/highscore.txt was not found!");
        }
        
    }

    
    // returns true if this is a new high score, false if not
    public boolean addScore(int score, boolean isMachine, String name) {
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] == null) {
                scores[i] = new Score(score, isMachine, name);
                save();
                return true;
            }
            if (score > scores[i].score) {
                for (int j = scores.length-1; j > i; j--)
                    scores[j] = scores[j-1];
                scores[i] = new Score(score, isMachine, name);
                save();
                return true;
            }
        }
        return false;
    }

    
    // get a high score list to display later
    public List<String> getDisplayStrings() {
	List<String> displayList = new ArrayList<String>();
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] == null) break;
            String line = Integer.toString(i+1) + ". ";
        if (i < 9) line = " " + line;
        displayList.add(line + scores[i].menuFormat);
    }
    return displayList;
}


public void save() {
    try {
        PrintWriter writer = new PrintWriter(fileName);
        for (Score s : scores) {
            if (s == null) break;
            writer.println(s.score + ":" + s.isMachine + ":" + s.name);
        }
        writer.close();
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }
}


private class Score {
    public int score;
    public boolean isMachine;
    public String name, menuFormat;
    
    public Score(int score, boolean isMachine, String name) { 
        this.score = score;
        this.isMachine = isMachine;
        if (name == null || name.length() < 1)
            this.name = (isMachine) ? "Machine" : "User";
        else 
            this.name = name;

        menuFormat = this.name;
            while(menuFormat.length() < 15) menuFormat += ".";
            menuFormat += " " + Integer.toString(score);
	}
    }
}
