package flappyBird.sprites;

import java.awt.Image;

public class Digit extends Sprite {

	public Digit(String fileName) {
	    x = y = 0;
	    readImage(fileName);
	}

	public Digit(Image image) {
	    x = y = 0;
	    setImage(image);
	}

}
