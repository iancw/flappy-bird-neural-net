package flappyBird.sprites;

import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * Created by austin on 1/21/15.
 */

/*
 * Note (3/30/15) Trying to go for a multi-layered background to make things
 * more appealing to the player. Currently starting with only two layers but
 * hopefully will work up to more - Davidson
 */
public class Background {

    private ArrayList<Bglayer> layers;

    public Background() {
	// Layer one (Back of layer stack)
	// Currently just a blue background (sky)
	layers = new ArrayList<Bglayer>();
	Bglayer layerZero = new Bglayer("img/background.png");
	layerZero.setScroll(false);
	layers.add(layerZero);

	// Layer two
	// Currently consists of trees and towers
	// Note that technically a "third" layer is added..
	// ..this is because this layer scrolls, repeating adjacently	
	Bglayer layerOne = new Bglayer("img/towers2.png");
	layerOne.setY(409);
	layerOne.setScrollSpeed((float) 0.185);
	layers.add(layerOne);
	Bglayer layerOneLag = new Bglayer(layerOne.getImage());
	layerOneLag.setScrollSpeed((float) 0.185);
	layerOneLag.setY(409);
	layers.add(layerOneLag);
	
	Bglayer layerTwo = new Bglayer("img/background2v1.png");
	layerTwo.setY(305);
	layerTwo.setScrollSpeed((float) 0.2);
	layers.add(layerTwo);
	Bglayer layerTwoLag = new Bglayer(layerTwo.getImage());
	layerTwoLag.setScrollSpeed((float) 0.2);
	layerTwoLag.setY(305);
	layers.add(layerTwoLag);
	

	
	Bglayer layerThree = new Bglayer("img/clouds.png");
	layerThree.setY(0);
	layerThree.setScrollSpeed((float) 0.2);
	layers.add(layerThree);
	Bglayer layerThreeLag = new Bglayer(layerThree.getImage());
	layerThreeLag.setScrollSpeed((float) 0.2);
	layerThreeLag.setY(0);
	layers.add(layerThreeLag);



    }

    public void render(Graphics2D g) {
	for (Bglayer layer : layers) {
	    layer.render(g);
	}
    }

    public void update() {
	for (Bglayer layer : layers) {
	    layer.update();
	}
    }

}
