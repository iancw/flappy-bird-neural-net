package flappyBird.sprites;

import flappyBird.game.Enums;

import javax.imageio.ImageIO;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static java.util.Collections.sort;

/**
 * Collidable, Background, and Bird extend Sprite directly Ground and Pipes
 * extend Collidable
 *
 * img/Bird/wallDeath sprites from "Pow Studios" website says there are free as
 * long as they are given credit
 * http://powstudios.com/content/smoke-animation-pack-1
 *
 * Note that, in java, the origin for the coordinates (x,y) is in the top left
 * corner. i.e. The positive x axis points rightward, and the positive y axis
 * points downward.
 */

@SuppressWarnings("SpellCheckingInspection")
public class Sprite {

    private Image image;
    public int x, y;
    protected int width, height;
    public String spriteName = "Sprite";

    // Holds all ArrayList<Image> animations
    protected static final HashMap<AnimType, ArrayList<Image>> animImages = new HashMap<AnimType, ArrayList<Image>>();


    // Sets a Sprite's image instance to an Image from a file whose name is
    // given in the string parameter
    public void readImage(String fileName) {
	try {
	    image = ImageIO.read(new File(fileName));
	} catch (IOException ioe) {
	    ioe.printStackTrace();
	}
	width = image.getWidth(null);
	height = image.getHeight(null);
    }


    public Image loadImage(String fileName) {
        Image img = null;
        try {
            img = ImageIO.read(new File(fileName));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return img;
    }

    // Draws a Sprite's image instance to a Graphics2D object
    public void render(Graphics2D g) {
	g.drawImage(image, x, y, null);
    }

    public static void loadAnimations() {
	animImages.put(AnimType.Flap, loadAnim("Bird/blue/", "flap"));
	animImages.put(AnimType.wallDeath, loadAnim("Bird", "wallDeath"));
    }

    // Throws AnimDirectoryException when a path/file is bad when loading Anim
    // images.
    public static ArrayList<Image> loadAnim(String sName, String aName) {
	ArrayList<Image> generalAnim = new ArrayList<Image>();
	try {
	    File dir = validateAnimDirectory(sName, aName);
	    for (File file : alphaListFiles(dir))
		generalAnim.add(ImageIO.read(file));
	} catch (AnimDirectoryException e) {
	    e.printStackTrace();
	    System.exit(1);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return generalAnim;
    }

    // Returns all of the files in a directory in alphabetical order
    public static File[] alphaListFiles(File file) {
	File[] files = file.listFiles();
	Arrays.sort(files, new Comparator<File>() {
	    @Override
	    public int compare(File file, File t1) {
		return file.toString().compareTo(t1.toString());
	    }
	});
	return files;
    }

    // Currently only validates Anim img directories containing png, jpg, and
    // jpg files
    public static File validateAnimDirectory(String spriteName, String animName)
	    throws AnimDirectoryException {
	File dir = new File("img/" + spriteName + "/" + animName);
	if (dir == null || !dir.exists())
	    throw new AnimDirectoryException("Directory " + dir + " doesn't exist.");
	if (!dir.isDirectory())
	    throw new AnimDirectoryException(dir.toString() + " is not a directory.");
	for (File file : dir.listFiles()) {
	    if (file.isDirectory())
		continue;
	    String name = file.toString();
	    if (name.endsWith("png") || name.endsWith("jpg") || name.endsWith("jpeg"))
		return dir;
	}
	throw new AnimDirectoryException("Directory " + dir.toString()
		+ " doesn't have any png or jpg files.");
    }

    // An exception specifically for loading images for Anim
    static class AnimDirectoryException extends Exception {
        private static final long serialVersionUID = 1L;
	public AnimDirectoryException(String message) {
	    super(message);
	}
    }


    public String getSpriteName() {
	return spriteName;
    }

    public int getX() {
	return x;
    }


    public int getY() {
	return y;
    }


    public int getWidth() {
	return width;
    }

    public void setWidth(int w) {
        width = w;
    }

    public void setHeight(int h) {
        height = h;
    }

    public int getHeight() {
	return height;
    }


    public Image getImage() {
	return image;
    }


    public void setImage(Image input) {
	image = input;
        if (image != null) {
            width = image.getWidth(null);
            height = image.getHeight(null);
        }
    }

}
