package flappyBird.sprites.menu;

import java.awt.event.MouseEvent;
import java.awt.*;


import flappyBird.game.Enums;
import flappyBird.sprites.Sprite;


/**
 * @author Stephen Pilcher Used to create buttons for the main menu.
 */

public class MButton extends Sprite{

    private Enums.Buttons bType;
    private boolean isHovering, isPressed;
    private Sprite background;
    // Non-hover at 0, hover at 1
    private Image[] imageList;
    // Non-pressed at 0, pressed at 1
    private static Image[] bgImages;
    private boolean noBg = false;
    

    public MButton(String fileName, int x, int y, int w, int h) {
        noBg = false;
        if (bgImages == null) loadBG();
        initBG(y);
	readImage(fileName); 
        this.x = x + (background.getWidth() - getWidth())/2;
        this.y = y + (background.getHeight() - getHeight())/2;
        imageList = new Image[2];
        imageList[0] = getImage();
        isHovering = isPressed = false;
    }


    public MButton(String fileName, int x, int y) {
        noBg = false;
        if (bgImages == null) loadBG();
        initBG(y);
	readImage(fileName);
        imageList = new Image[2];
        imageList[0] = getImage();
	this.x = x;
	this.y = y;
        isHovering = isPressed = false;
    }


    //For the confirm menu
    public MButton(String fileName, int x, int y, boolean noBackground) {
        noBg = true;
        readImage(fileName);
        imageList = new Image[2];
        imageList[0] = getImage();
	this.x = x;
	this.y = y;
        isHovering = isPressed = false;
        background = new Sprite();
        background.setImage(getImage());
        background.x = x;
        background.y = y;
        background.setWidth(getWidth());
        background.setHeight(getHeight());
    }


    public void centerTextOnButton() {
        x = background.x-2 + (background.getWidth() - getWidth())/2;
        y = background.y-2 + (background.getHeight() - getHeight())/2;
    }


    public void initBG(int y) {
        background = new Sprite();
        background.x = (Menu.WIDTH - Menu.BUTTON_BG_WIDTH)/2;
        background.y = y;
        background.setImage(bgImages[0]);
    }

    private void loadBG() {
        bgImages = new Image[2];
        bgImages[0] = loadImage("img/menu/button/button_bg.png");
        bgImages[1] = loadImage("img/menu/button/button_bg_pressed.png");
    }


    /**
     * determines if the event coordinate is inside the instance
     * @param x event x
     * @param y event y
     * @return true if inside, false otherwise
     */
    protected boolean isInside(int x1, int y1) {
	return (x1 >= background.x && 
	        y1 >= background.y && 
                x1 <= background.x + background.getWidth() &&
                y1 <= background.y + background.getHeight());
    }
    

    public void setType(Enums.Buttons input){
	bType = input;
    }
    

    public Enums.Buttons getType(){
	return bType;
    }


    public void updateHover(int mouseX, int mouseY) {
        if (imageList[1] == null) return;
        if (isHovering && !isInside(mouseX, mouseY)) {
            isHovering = false;
            setImage(imageList[0]);
        }
        else if (!isHovering && isInside(mouseX, mouseY)) {
            isHovering = true;
            setImage(imageList[1]);
        }
    }


    public void updatePressed(int mouseX, int mouseY) {
        if (isInside(mouseX, mouseY)) {
            isPressed = true;
            if (!noBg)
                background.setImage(bgImages[1]);
        }
    }


    public boolean clickedCheck(int mouseX, int mouseY) {
        if (isPressed) {
            isHovering = false;
            isPressed = false;
            setImage(imageList[0]);
            background.setImage(bgImages[0]);
            return isInside(mouseX, mouseY);
        }
        return false;
    }


    public void render(Graphics2D g) {
        if (!noBg)
            g.drawImage(background.getImage(), background.getX(), background.getY(), null);
        g.drawImage(getImage(), getX(), getY(), null);
    }


    public void setHoverImg(String input){
        imageList[1] = loadImage(input);
    }

}
