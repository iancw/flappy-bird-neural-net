package flappyBird.sprites.menu;

import flappyBird.game.Enums;

public class PauseMenu extends Menu {


    public PauseMenu(Enums.Menu input){
	
	// inline menu settings
	inlineHeight = (WIDTH / 8) + 20;
	inlineWidth = (HEIGHT / 4) - 20;
	inlineSpace = 60;
	
	// menu background
	mbg = new MenuBackground("img/menu/pause/p_bg.png", -4, 0);
	
			
	// add buttons
	
	// resume button
	addButtonInline("img/menu/pause/resume.png", "img/menu/pause/resume_h.png", Enums.Buttons.RESUMEB);
	
	// menu button
	addButtonInline("img/menu/pause/menubutton.png", "img/menu/pause/menubutton_h.png", Enums.Buttons.MENUB);

        centerButtonText();
	
	// add menu type
	setmType(input);

	
    }

    

}
