package flappyBird.sprites.menu;


import flappyBird.game.Enums;

public class MainMenu extends Menu {
    

    public MainMenu(Enums.Menu input){
	// inline menu settings
	inlineHeight = WIDTH / 8;
	inlineWidth = (HEIGHT / 7);
	inlineSpace = 20;
	
	// menu background
	mbg = new MenuBackground("img/menu/main/m_bg.png");
		
	// add buttons
	addButtonInline("img/menu/main/qplay.png", "img/menu/main/qplay_h.png", Enums.Buttons.PLAYB);
	// TODO: make these buttons actually do something other than start the game
	addButtonInline("img/menu/main/loadnetwork.png", "img/menu/main/loadnetwork_h.png", Enums.Buttons.LOADB);
	addButtonInline("img/menu/main/settings.png", "img/menu/main/settings_h.png", Enums.Buttons.SETTINGSB);
	addButtonInline("img/menu/main/highscores.png", "img/menu/main/highscores_h.png", Enums.Buttons.HIGH_SCOREB);

        centerButtonText();
	
	// set menu type
	setmType(input);
    }

}
