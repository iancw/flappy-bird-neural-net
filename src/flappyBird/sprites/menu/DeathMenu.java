package flappyBird.sprites.menu;

import flappyBird.game.Enums;
import flappyBird.sprites.Score;
import flappyBird.sprites.Digit;

import java.awt.Graphics2D;
import java.util.List;
import java.util.ArrayList;
import java.awt.Font;


public class DeathMenu extends Menu{

    private Score scoreDisplay;
    private int scoreX, scoreY;
    public boolean reset = false;
    private Font font;


    public DeathMenu(Enums.Menu input){
	
	// inline menu settings
	inlineHeight = (WIDTH / 8) + 20;
	inlineWidth = (HEIGHT / 4) - 20;
	inlineSpace = 20;
        scoreX = 250;
        scoreY = 125;
	
	// menu background
	mbg = new MenuBackground("img/menu/dead/d_bg.png", 25, 90);
	addButton("img/menu/main/highscores.png", "img/menu/main/highscores_h.png", 100, 250, Enums.Buttons.HIGH_SCOREB);
	addButton("img/menu/pause/menubutton.png", "img/menu/pause/menubutton_h.png", 100, 250+BUTTON_BG_HEIGHT+inlineSpace, Enums.Buttons.MENUB);
        centerButtonText();

        font = new Font("Open Sans", Font.BOLD, 22);
	
	setmType(input);

        scoreDisplay = new Score();
    }

    public void setScore(List<Digit> digs) {
        scoreDisplay.setScoreDigits(digs);
        scoreDisplay.moveTo(scoreX, scoreY);
        reset = false;
    }

    @Override
    public void render(Graphics2D g) {
        g.setFont(font);
        mbg.render(g);
        g.drawString("Click to play again.", 85, 220);
        for (MButton b : mb) b.render(g);
        scoreDisplay.render(g);
    }

    @Override
    public void updateButtonPress(int mouseX, int mouseY) {
        for (MButton b : mb) 
            b.updatePressed(mouseX, mouseY);
        if (!mb.get(0).isInside(mouseX, mouseY) && !mb.get(1).isInside(mouseX, mouseY))
            reset = true;
    }

    public void updateMenuScore(int score) {
        scoreDisplay.updateScore(score);
        scoreDisplay.moveTo(scoreX, scoreY);
    }
}

