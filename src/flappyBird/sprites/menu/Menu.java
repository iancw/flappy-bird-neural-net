package flappyBird.sprites.menu;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ArrayList;

import flappyBird.game.Enums;


public class Menu {

    protected MenuBackground mbg;
    protected List<MButton> mb;
    protected int inlineWidth, inlineHeight, inlineSpace;
    protected Enums.Menu mType;

    public static final int HEIGHT = 720;
    public static final int WIDTH = HEIGHT * 10 / 16;
    public static final int BUTTON_BG_WIDTH = 300;
    public static final int BUTTON_BG_HEIGHT = 75;
    public static final int MENU_Y_START = 36;
    
    public Menu() {
	mb = new ArrayList<MButton>();
	//mType = input;
	inlineWidth = 0;
	inlineHeight = 0;
	inlineSpace = 0;
    }
    
    public void render(Graphics2D g){
	if(mbg != null)
	    mbg.render(g);
	for(MButton b: mb)
            b.render(g);
    }
    

    public void centerButtonText() {
        for (MButton button : mb) {
            button.centerTextOnButton();
        }
    }
    
    /* 
     * addButton methods 
     * There are two addButton methods, each with their own purpose 
     * addButton(String, int, int, Enums.Buttons) is used if the mouseClick event does nothing except return true/false 
     * addButton(MButton) is used otherwise
    */
    
    protected void addButton(String img, String hover, int x, int y, Enums.Buttons type){
        MButton button = new MButton(img, x, y);
	button.setType(type);    
	button.setHoverImg(hover);
	mb.add(button);
    }


    protected void addNoBgButton(String img, String hover, int x, int y, Enums.Buttons type) {
        MButton button = new MButton(img, x, y, true);
	button.setType(type);    
	button.setHoverImg(hover);
	mb.add(button);
    }
    
    protected void addButton(MButton button){
	mb.add(button);
    }
    
    /* 
     * addButtonInLine() makes it easy to setup a menu
     * that has all its buttons inline vertically
     * All you need to do is specify inlineWidth and inlineHeight
     * in the menu you are creating
    */
    protected void addButtonInline(String img, String hover, Enums.Buttons type) {	
        int yPos;
        if (mb.isEmpty())
            yPos  = inlineSpace + MENU_Y_START;
        else
            yPos = mb.get(mb.size()-1).y + BUTTON_BG_HEIGHT + inlineSpace;
	MButton button = new MButton(img, inlineWidth, yPos);
	button.setType(type);    
	button.setHoverImg(hover);
	mb.add(button);
    }

 
    /*
     * checkButtons method
     * This checks all of the buttons in arraylist mb to detect which
     * button was clicked and return it
     * This also resets the image of a button if the cursor is no longer
     * hovering over it
     */
    public void updateButtonHover(int mouseX, int mouseY) {
	for(MButton b : mb) 
            b.updateHover(mouseX, mouseY);
    }


    public void updateButtonPress(int mouseX, int mouseY) {
        for (MButton b : mb) 
            b.updatePressed(mouseX, mouseY);
    }


    public MButton checkButtonRelease(int mouseX, int mouseY) {
        for (MButton button : mb) {
            if (button.clickedCheck(mouseX, mouseY))
                return button;
        }
        return null;
    }


    public Enums.Menu getmType(){
	return mType;
    }
    

    protected final void setmType(Enums.Menu input){
	mType = input;
    }
    
    
}
