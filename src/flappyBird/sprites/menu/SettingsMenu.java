package flappyBird.sprites.menu;

import flappyBird.game.Enums;

public class SettingsMenu extends Menu{
    
    public SettingsMenu(Enums.Menu input){
	// inline menu settings
	inlineHeight = WIDTH / 8;
	inlineWidth = (HEIGHT / 7);
	inlineSpace = 20;
	
	// menu background
	mbg = new MenuBackground("img/menu/settings/s_bg.png");
		
	// menu buttons
	addButton("img/menu/confirm/confirm.png", "img/menu/confirm/confirm_h.png", 200, 600, Enums.Buttons.MENUB);
        centerButtonText();
	
	// set menu type
	setmType(input);
    }
    
}
