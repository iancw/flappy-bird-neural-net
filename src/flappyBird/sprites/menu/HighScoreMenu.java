package flappyBird.sprites.menu;

import flappyBird.game.Enums;
import flappyBird.sprites.Sprite;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;


public class HighScoreMenu extends Menu {

    private Sprite title;
    private List<String> highScoreText;
    private Font font;


    public HighScoreMenu(Enums.Menu input) {
        inlineHeight = WIDTH / 8;
        inlineWidth = (HEIGHT / 7);
        inlineSpace = 25;
	mbg = new MenuBackground("img/menu/main/m_bg.png");
        initTitle();
        highScoreText = new ArrayList<String>();
	addButton("img/menu/pause/menubutton.png", "img/menu/pause/menubutton_h.png", 
                (WIDTH - BUTTON_BG_WIDTH)/2, HEIGHT-MENU_Y_START-inlineSpace-BUTTON_BG_HEIGHT, Enums.Buttons.MENUB);
        centerButtonText();

        try { 
            font = Font.createFont(Font.TRUETYPE_FONT, new File("font/UbuntuMono-Bold.ttf")); 
        } catch (FontFormatException e1) { 
           e1.printStackTrace(); 
        } catch (IOException e1) { 
            e1.printStackTrace(); 
        }

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(font);

        font = new Font("Ubuntu Mono", Font.BOLD, 26);
    }


    private void initTitle() {
        title = new Sprite();
        title.readImage("img/menu/main/highscores.png");
        title.y = MENU_Y_START + inlineSpace;
        title.x = (WIDTH - title.getWidth())/2;
    }


    public void setScoreList(List<String> scoreList) {
        highScoreText = new ArrayList<String>();
        for (String i : scoreList) highScoreText.add(i);
    }


    @Override
    public void render(Graphics2D g) {
        mbg.render(g);
        g.setFont(font);
        title.render(g);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        int yPos = title.y + title.getHeight() + inlineSpace + 10;
        int xPos = 75;
        for (String line : highScoreText) {
            g.drawString(line, xPos, yPos);
            yPos += inlineSpace;
        }
        mb.get(0).render(g);
    }


    

}
