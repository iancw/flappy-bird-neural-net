package flappyBird.sprites.menu;

import flappyBird.sprites.Sprite;

public class MenuBackground extends Sprite {
    
    public MenuBackground(String fileName) {	
	readImage(fileName); 
	x = 0; 
	y = 0; 
    }
    
    public MenuBackground(String fileName, int ix, int iy) {
	readImage(fileName); 
	x = ix; 
	y = iy; 
    }

}
