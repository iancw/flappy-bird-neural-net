package flappyBird.sprites.menu;

import flappyBird.game.Enums;

public class ConfirmMenu extends Menu{
    
    public ConfirmMenu(Enums.Menu input){

	mbg = new MenuBackground("img/menu/confirm/c_bg.png", 25 , 90);	
		
	addNoBgButton("img/menu/confirm/confirm.png", "img/menu/confirm/confirm_h.png", 100, 350, Enums.Buttons.CONFIRMB);
	addNoBgButton("img/menu/confirm/cancel.png", "img/menu/confirm/cancel_h.png", 250, 350, Enums.Buttons.CANCELB);
	
	setmType(input);
	
    }
    
    
    
    
}
