package flappyBird.sprites;

import flappyBird.game.Enums;
import flappyBird.game.Enums.BirdState;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Gravity = how much velocity will increase every update cycle Velocity = how
 * many pixels the y instance will change every update cycle flapVelocity = how
 * much how fast the bird moves upward when if "flaps"
 *
 * Score is Bird instance so that each Bird instance can have it's own score if
 * multiple instances are run at once. This also allows easy scoring in the
 * Pipes birdCollided(Bird bird) method, since the score is to be increased
 * whenever the bird "collides" with the empty space between 2 pipes in a Pipes
 * instance.
 *
 * Gravity and flapVelocity are static so that if there are multiple instances
 * of bird they will not need to be stored on a per-instance basis.
 *
 */

public class Bird extends Sprite {

    // These can be changed to modify the feel/difficulty of the game
    // Max velocity is the fastest the Bird could move due to falling
    private static final double GRAVITY = 0.6, FLAP_VELOCITY = -9.8, MAX_VELOCITY = Math
	    .sqrt(2 * GRAVITY * 565);
    private static Image original;

    private Anim anim;
    private static Score scoreDisplay;
    private double velocity, angle;
    private static int yInitial = 160;
    private static int xInitial = 100;
    private double deathTime = 0;
    public int score;
    private Enums.BirdState state;
    

    public Bird(String fileName) {
	readImage(fileName);
	original = getImage();
	anim = new Anim(this);
	velocity = 0;
	angle = 0;
	x = xInitial;
	y = yInitial;
	score = 0;
	scoreDisplay = new Score();
	state = Enums.BirdState.ALIVE;
    }


    public Bird() {
        setImage(original);
        anim = new Anim(this);
        velocity = 0;
        angle = 0;
        x = xInitial;
        score = 0;
        state = Enums.BirdState.ALIVE;
        scoreDisplay = new Score();
        y = 150 + (int)(315*Math.random());
    }


    // Renders the bird at the appropriate angle
    @Override
    public void render(Graphics2D g) {
	if (state == Enums.BirdState.DYING) {
	    g.drawImage(getImage(), x, y, null);
	    return;
	}
	else if (state == Enums.BirdState.DEAD)
	    return;
	
	AffineTransform old = g.getTransform();
	AffineTransform transform = new AffineTransform();
	transform.rotate(angle, x + width / 2, y + height / 2);
	g.transform(transform);
	g.drawImage(getImage(), x, y, null);
	g.setTransform(old);
    }
    
    public void renderScore(Graphics2D g) {
	if (score != scoreDisplay.getDisplayedScore())
	    scoreDisplay.updateScore(score);
	scoreDisplay.render(g);
    }

    public double getVelocity() {
	return velocity;
    }

    public double getAngle() {
	return angle;
    }

    public void update() {
	velocity += GRAVITY;
	updateAngle();
	y += (int) Math.round(velocity);
	if (y < 0)
	    velocity = y = 0;
    }
    
    public void updateAndRespawn(boolean safeToSpawn) {
	if(safeToSpawn)
	    resetAfterDeath();
	update();
    }

    // Max plausible rotation of pi/2 and defaults pi/16 upward
    // Magnitude relates quadratically to prevent "twitching" when velocity
    // hovers around 0
    public void updateAngle() {
	angle = ((velocity < 0) ? -Math.PI / 2 : Math.PI / 2)
		* Math.pow(velocity / MAX_VELOCITY, 2) - Math.PI / 16;
	if (Math.abs(angle) > Math.PI / 2)
	    angle = (angle > 0) ? Math.PI / 2 : -Math.PI / 2;
    }

    // If bird is falling, on flap it's velocity changes to being upward at
    // flapVelocity
    // If the bird is already going up, it's velocity magnitude increases by
    // flapVelocity
    public void flap() {
	velocity = (velocity >= 0) ? FLAP_VELOCITY : velocity + FLAP_VELOCITY;
	animate(AnimType.Flap);
    }

    public void reset() {
	setImage(original);
	score = 0;
	x = xInitial;
	changeState(Enums.BirdState.ALIVE);
	y = 150 + (int)(315*Math.random());
	velocity = -10;
    }

    // Used to stop the animation. Primarily used for when the game is paused
    public void stopAnim() {
	anim.stop();
    }

    // Used to make animation easier and to ensure bird doesn't try to animate
    // flapping while dying
    protected void animate(AnimType type) {
	switch (type) {
	case Flap:
	    if (state != Enums.BirdState.ALIVE)
		return;
	    anim.play(type, true);
	    break;
	case wallDeath:
	    angle = 0;
	    anim.play(type, false);
	    break;
	}
    }
    
    public void doCollision(Collidable col) {
	if(col instanceof Pipes) {
	    if (this.x < col.x - (9 * this.width / 10)) {
		changeState(Enums.BirdState.DYING);
		this.animate(AnimType.wallDeath);
	    } else
		changeState(Enums.BirdState.DEAD);
	} else if(col instanceof Ground) {
	    changeState(Enums.BirdState.DEAD);
	}
    }
    
    public void updateScore(Collidable col) {
	if(col instanceof Pipes) {
	    if (state == Enums.BirdState.ALIVE && x + width >= col.x
		    && x + width < col.x - Collidable.velocity && y >= col.y + 450
		    && y <= col.y + 600 - height) {
		score++;
	    }
	}
    }
    
    protected void changeState(Enums.BirdState newState) {
	if(newState == Enums.BirdState.DEAD) {
	    deathTime = System.currentTimeMillis();
	} else
	    deathTime = 0;
	state = newState;
    }
    
    private void resetAfterDeath() {
	if(state == BirdState.DEAD) {
	    //System.out.println("Rez");
	    reset();
	}
    }
    
    public Enums.BirdState getBirdState() {
	return state;
    }
    
    public Score getScoreDisplay() {
	return scoreDisplay;
    }
}
