package flappyBird.sprites;

import java.util.List;
import java.util.ArrayList;
import java.awt.*;

public class Score {

    private int displayedScore;
    private List<Digit> scoreDigits;

    private static Digit[] digits;
    private static final int SCREEN_WIDTH = 450;
    private static final int Y_POS = 650;

    public Score() {
	scoreDigits = new ArrayList<Digit>();
	digits = new Digit[10];
	digits[0] = new Digit("img/numbers/zero.png");
	digits[1] = new Digit("img/numbers/one.png");
	digits[2] = new Digit("img/numbers/two.png");
	digits[3] = new Digit("img/numbers/three.png");
	digits[4] = new Digit("img/numbers/four.png");
	digits[5] = new Digit("img/numbers/five.png");
	digits[6] = new Digit("img/numbers/six.png");
	digits[7] = new Digit("img/numbers/seven.png");
	digits[8] = new Digit("img/numbers/eight.png");
	digits[9] = new Digit("img/numbers/nine.png");
	displayedScore = 0;
	updateScore(0);
    }

    public int getDisplayedScore() {
	return displayedScore;
    }

    public List<Digit> getDigitList() {
        return scoreDigits;
    }

    public void render(Graphics2D g) {
	for (Digit dig : scoreDigits)
	    dig.render(g);
    }

    public void updateScore(int newScore) {
        boolean ret;
        if (displayedScore != newScore) ret = true;
	displayedScore = newScore;
	scoreDigits.clear();
	String str = Integer.toString(newScore);
	for (int i = 0; i < str.length(); i++) {
	    Digit temp = new Digit(digits[(int) str.charAt(i) - 48].getImage());
	    scoreDigits.add(temp);
	}
	setScorePosition();
    }

    public void moveTo(int x,int y) {
        int dx = x - scoreDigits.get(0).x;
        int dy = y - Y_POS;
        for (Digit dig : scoreDigits) {
            dig.x += dx;
            dig.y += dy;
        }
    }

    public void setScoreDigits(List<Digit> digs) {
        scoreDigits = new ArrayList<Digit>();
        for (Digit d : digs) 
            scoreDigits.add(d);
    }

    public void setScorePosition() {
	int x = (SCREEN_WIDTH - totalWidth()) / 2;
	for (Digit dig : scoreDigits) {
	    dig.x = x;
	    dig.y = Y_POS;
	    x += dig.getImage().getWidth(null) + 2;
	}
    }

    public int totalWidth() {
	int wid = 2 * (scoreDigits.size() - 1);
	for (Digit dig : scoreDigits)
	    wid += dig.getImage().getWidth(null);
	return wid;
    }


}
