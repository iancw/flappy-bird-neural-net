package flappyBird.sprites;

import flappyBird.game.Enums;

import java.awt.Image;

/*
 // Animation class that runs in a thread
 // *  Sprite: the sprite that is to be animated
 // *  genAnim: the animation stored in an ArrayList
 // *  isCircular: whether or not the animation is supposed to
 //    return to its original image

 Anim now implements Runnable
 I removed the inner class entirely and moved run() method into Anim
 I added methods for thread safety and to ensure only 1 Anim thread per sprite at any given time
 All animated images share the static HashMap<AnimType, ArrayList<Image>> animImages in the Sprite class
 Each Sprite extending class should get a Anim instance in order to be animated

 To make a new animation
 1) Place the source images appropriately in /img/spriteName/animName
 2) Add the new animation type to enum AnimType
 3) Add another put call for the new source images in loadAnimations() in the Sprite class
 4) If the sprite is NOT Bird, create an Anim instance for the sprite object
 5) Call play when appropriate
 */

public class Anim implements Runnable {

    private Sprite sprite;
    private boolean isCircular;
    private AnimType animType;
    private Thread thread;
    private Image prevImage;

    public Anim(Sprite sprite) {
	this.sprite = sprite;
	prevImage = sprite.getImage();
    }

    public void play(AnimType type, boolean inCircular) {
	stop();
	animType = type;	
	isCircular = inCircular;
	thread = new Thread(this);
	thread.start();
    }

    // Stops previous Anim thread on sprite if it is still running
    // Ensures proper gameState and Sprite.image values
    public void stop() {
	if (thread == null || !thread.isAlive())
	    return;
	thread.interrupt();
	if (isCircular)
	    sprite.setImage(prevImage);
	if (animType == AnimType.wallDeath)
	    ((Bird)sprite).changeState(Enums.BirdState.DEAD);
	try {
	    thread.join();
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    // (Where the magic happens)
    // This is pretty much the same run method, just moved it into Anim
    public void run() {
	long time1, time2, difference;

	for (Image img : Sprite.animImages.get(animType)) {
	    if (thread.isInterrupted())
		return;
	    if (animType == AnimType.wallDeath) {
		// rotate the image so that it appears to splash against the wall
		sprite.x -= (Sprite.animImages.get(animType).get(0).getWidth(null) - sprite.width);
		sprite.y -= (Sprite.animImages.get(animType).get(0).getHeight(null) - sprite.height) / 2;
	    }
	    time1 = System.currentTimeMillis();
	    sprite.setImage(img);
	    try {
		time2 = System.currentTimeMillis();
		difference = time2 - time1;
		Thread.sleep(100 - difference); // ~10 fps on animations
	    } catch (InterruptedException e) {
		// No stack trace, the thread will be intentionally interrupted
		// if it needs to restart/change animations
		return;
	    }
	}

	// There was a Thread.sleep(~100ms) here in case it was doing something
	// I did not anticipate
	// I removed it since thread has already slept 100 ms at the bottom of
	// the for loop
	if (animType == AnimType.wallDeath)
	    ((Bird)sprite).changeState(Enums.BirdState.DEAD);
	if (!thread.isInterrupted() && isCircular)
	    sprite.setImage(prevImage);
    }

}
