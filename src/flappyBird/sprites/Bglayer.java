package flappyBird.sprites;

import java.awt.Graphics2D;
import java.awt.Image;

// Generic background layer class
// Note: fX corresponds to the x coordinate of layer
// It was switched from int to float to fine tune the..
// ..scrolling speed. If anyone has a more efficient way
// ..to accomplish this fine tuning, please do so!

public class Bglayer extends Sprite {

    // toScroll is exactly what it sounds like
    // Defaulted to true since most of the background probably will be moving
    private boolean toScroll = true;

    private float fX;
    private float scrollSpeed = 1;

    public Bglayer(String fileName) {
	readImage(fileName);
	fX = 0;
	y = 0;
	width = getImage().getWidth(null);
	height = getImage().getHeight(null);
    }

    // Used the same technique as for the ground
    // Avoids calling the same file twice
    public Bglayer(Image img) {
	setImage(img);
	width = getImage().getWidth(null);
	height = getImage().getHeight(null);
	fX = width;
    }

    public void update() {
	if (toScroll) {
	    fX += -2 * scrollSpeed;
	    if (fX < -1 * width)
		fX += 2 * width;
	}

    }

    public void render(Graphics2D g) {
	g.drawImage(this.getImage(), (int) fX, y, null);
    }

    public void setScroll(boolean input) {
	toScroll = input;
    }

    public void setX(int inputX) {
	fX = inputX;
    }

    public void setY(int inputY) {
	y = inputY;
    }

    public void setScrollSpeed(float input) {
	scrollSpeed = input;
    }

}